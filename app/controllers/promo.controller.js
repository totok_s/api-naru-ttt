/*var multer = require('multer');
var uuid = require('node-uuid');

var storage = multer.diskStorage({
    destination: './uploads/',
    limits: {
        fileSize: 4096
    },
    filename: function(req, file, callback) {
        var fileOriginal = file.originalname;
        var reverseFile = fileOriginal.split('').reverse().join('');
        var splitEktension = reverseFile.split('.')[0];
        var extension = splitEktension.split('').reverse().join('');
        callback(null, (uuid.v4() + '.' + extension));
    }
});

var upload = multer({
    storage: storage
});*/

var Promo = require('../models/promo.model.js');

exports.create = function(req, res) {
    // Create and Save a new Note
    /*if(!req.body.content) {
        res.status(400).send({message: "Note can not be empty"});
    }*/

    var promo = new Promo({newsPromoName: req.body.newsPromoName, newsPromoImage: req.body.newsPromoImage, startDate: req.body.startDate, outDate: req.body.outDate, status: req.body.status, newsCategory: req.body.newsCategory, newsPromoNumber: req.body.newsPromoNumber, newsPromolocation: req.body.newsPromolocation, newsPromoDescription: req.body.newsPromoDescription});

    promo.save(function(err, data) {
        //console.log(data);
        if(err) {
            //console.log(err);
            res.status(500).send({message: err});
        } else {
            res.status(200).send(data);
        }
    });
};


exports.findAll = function(req, res) {
    // Retrieve and return all notes from the database.
    Promo.find(function(err, promo){
        if(err) {
            res.status(500).send({message: err});
        } else {
            res.status(200).send(promo);
        }
    });
};

exports.findOne = function(req, res) {
    // Find a single note with a noteId
    Promo.findById(req.params.promoId, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not retrieve Promo with id " + req.params.promoId});
        } else {
            res.status(200).send(data);
        }
    });
};

exports.update = function(req, res) {
    // Update a note identified by the noteId in the request
    Promo.findById(req.params.promoId, function(err, promo) {
        if(err) {
            res.status(500).send({message: "Could not find a note with id " + req.params.promoId});
        }

        promo.newsPromoName = req.body.newsPromoName;
        promo.newsPromoImage=req.body.newsPromoImage;
        promo.startDate = req.body.startDate;
        promo.outDate = req.body.outDate;
        promo.status = req.body.status;
        promo.newsCategory = req.body.newsCategory;
        promo.newsPromoNumber = req.body.newsPromoNumber;
        promo.newsPromolocation = req.body.newsPromolocation;
        promo.newsPromoDescription = req.body.newsPromoDescription;

        promo.save(function(err, data){
            if(err) {
                res.status(500).send({message: "Could not update promo with id " + req.params.promoId});
            } else {
                res.status(200).send(data);
            }
        });
    });
};

exports.delete = function(req, res) {
    // Delete a note with the specified noteId in the request
    Promo.remove({_id: req.params.promoId}, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not delete promo with id " + req.params.id});
        } else {
            res.send({message: "promo deleted successfully!"})
        }
    });
};