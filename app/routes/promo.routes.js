module.exports = function(app) {
  var multer = require('multer');
  var uuid = require('node-uuid');
  const nconf = require('nconf')
  const fs = require('fs');
  const aws = require('aws-sdk');
  var arrName = [];

  var storage = multer.diskStorage({
    destination: './uploads/',
    limits: {
      fileSize: 4096
    },
    filename: function(req, file, callback) {
      var fileOriginal = file.originalname;
      var reverseFile = fileOriginal.split('').reverse().join('');
      var splitEktension = reverseFile.split('.')[0];
      var extension = splitEktension.split('').reverse().join('');
      var newName = uuid.v4();
      arrName.push(`${newName}.${extension}`);
      callback(null, (newName + '.' + extension));
    }
  });

  var upload = multer({
    storage: storage
  });


  var PromoLogic = require('../logics/PromoLogic.js');


  app.post('/promos', upload.array('newsPromoImage', 5), function(req, res) {
    var params = req.body;
    var listImage = [];
    arrName.map(v => {
      let path = `./uploads/${v}`;
      var bucket = "temanberbagi";
      var key = `news/${v}`;
      aws.config.update({
        "accessKeyId": "AKIAIGKUPUJWGDQPE45Q",
        "secretAccessKey": "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",
        "region": "ap-southeast-1"
      });
      var s3 = new aws.S3();
      var images = fs.createReadStream(path);

      const awsObj = {
        Bucket: bucket,
        Key: key,
        Body: images
      }
      listImage.push(`https://s3-ap-southeast-1.amazonaws.com/temanberbagi/news/${v}`);
      s3.putObject(awsObj, function(err, data) {
        if (err) {
          console.log("error upload images", err);
        } else {
          console.log("success upload images", data);
          fs.unlinkSync('./uploads/' + v);
        }
      })
    })
    params.newsPromoImage = listImage;
    console.log(arrName);
    arrName = [];

    PromoLogic.savePromo(params, function(promo) {

      return res.json(promo);

    });
  });

  app.get('/promos', function(req, res) {
    PromoLogic.findAllPromo(function(promos) {
      return res.json(promos);
    });
  });

  app.put('/promos/:promo_id', function(req, res) {
    PromoLogic.updatePromo(req.params.promo_id, req.body, function(promo) {
      return res.json(promo);
    });
  });

  app.delete('/promos/:promo_id', function(req, res) {
    var promo_id = req.params.promo_id;
    PromoLogic.deletePromo(promo_id, function(promos) {
      res.json(promos);
    });
  });

  app.get('/promos/:promo_id', function(req, res) {
    var id = req.params.promo_id;
    PromoLogic.findByIdPromo(id, function(promos) {
      res.json(promos);
    });
  });


}
