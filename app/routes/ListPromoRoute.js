module.exports = function(app) {
  var multer = require('multer');
  var uuid = require('node-uuid');
  const nconf = require('nconf')
  const fs = require('fs');
  const aws = require('aws-sdk');
  var arrName = [];

  var storage = multer.diskStorage({
    destination: './uploads/',
    limits: {
      fileSize: 4096
    },
    filename: function(req, file, callback) {
      var fileOriginal = file.originalname;
      var reverseFile = fileOriginal.split('').reverse().join('');
      var splitEktension = reverseFile.split('.')[0];
      var extension = splitEktension.split('').reverse().join('');
      var newName = uuid.v4();
      arrName.push(`${newName}.${extension}`);
      callback(null, (newName + '.' + extension));
    }
  });

  var upload = multer({
    storage: storage
  });


  var ListPromoLogic = require('../logics/ListPromoLogic.js');



  app.post('/:promo_id/listpromos', upload.array('listNewsPromoImage', 5), function(req, res) {
    var params = req.body;
    var idPromo = req.params.promo_id;
    var listImage = [];
    arrName.map(v => {
      let path = `./uploads/${v}`;
      var bucket = "temanberbagi";
      var key = `news/${v}`;
      aws.config.update({
        "accessKeyId": "AKIAIGKUPUJWGDQPE45Q",
        "secretAccessKey": "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",
        "region": "ap-southeast-1"
      });
      var s3 = new aws.S3();
      var images = fs.createReadStream(path);

      const awsObj = {
        Bucket: bucket,
        Key: key,
        Body: images
      }
      listImage.push(`https://s3-ap-southeast-1.amazonaws.com/temanberbagi/news/${v}`);
      // console.log(awsObj);
      s3.putObject(awsObj, function(err, data) {
        if (err) {
          console.log("error upload images", err);
        } else {
          console.log("success upload images", data);
          fs.unlinkSync('./uploads/' + v);
        }
      })
    })
    // params.id=  params.promo_id;
    params.listNewsPromoImage = listImage;
    console.log(arrName);
    arrName = [];
    params.promo_id = idPromo;
    ListPromoLogic.saveListPromo(params, function(listPromos) {

      return res.json(listPromos);

    });
  });

  app.get('/:promo_id/listpromos', function(req, res) {
    var promo_id = req.params.promo_id;

    ListPromoLogic.findAllByIdPromo(promo_id, function(listPromos) {
      return res.json(listPromos);
    });
  });

  app.get('/listpromos', function(req, res) {
    ListPromoLogic.findAllListPromo(function(listpromos) {
      return res.json(listpromos);
    });
  });

  app.get('/listpromos/:listPromos_id', function(req, res){
    var promo_id = req.params.listPromos_id;
    ListPromoLogic.findByIdPromos(promo_id, function(listPromos){
      return res.json(listPromos);
    });
  });

  app.put('/:promo_id/listpromos/:listPromos_id', function(req, res) {
    var params = req.body;
    var idPromo = req.params.promo_id;
    params.promo_id = idPromo;
    var id = req.params.listPromos_id;
    ListPromoLogic.updateListPromo(id, params, function(listPromos) {
      return res.json(listPromos);
    });
  });

  app.delete('/listpromos/:listPromos_id', function(req, res) {
    var params = req.params.promo_id;

    ListPromoLogic.deleteListPromo(params, function(listPromos) {
      res.json(listPromos);
    });
  });

}
