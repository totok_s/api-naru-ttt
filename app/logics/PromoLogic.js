const Promo = require('../models/promo.model');

var promoLogic = {
  //ambil semua company untuk admin
  findAllPromo: function(callback) {
    Promo.find(function(err, promos) {
      if (err) {
        return callback(err);
      }

      callback(promos);
    });
  },

  findByIdPromo: function(promo_id, callback) {
    Promo.findById(promo_id, function(err, res) {
      if (err) {
        callback({
          status: false,
          message: err
        });
        return;
      }

      callback({
        status: true,
        message: res
      });
    });
  },

  savePromo: function(params, callback) {
    var promo = new Promo({
      newsPromoName: params.newsPromoName,
      newsPromoImage: params.newsPromoImage,
      startDate: params.startDate,
      outDate: params.outDate,
      status: params.status,
      newsCategory: params.newsCategory,
      newsPromoNumber: params.newsPromoNumber,
      newsPromolocation: params.newsPromolocation,
      newsPromoDescription: params.newsPromoDescription
    });

    promo.save(function(err, promoS) {
      if (err) {
        return callback(err);
      }
      callback(promoS);
    });
  },


  updatePromo: function(promo_id, params, callback) {
    Promo.findById(promo_id, function(promo) {
      promo.newsPromoName = params.newsPromoName;
      promo.newsPromoImage = params.newsPromoImage;
      promo.startDate = params.startDate;
      promo.outDate = params.outDate;
      promo.status = params.status;
      promo.newsCategory = params.newsCategory;
      promo.newsPromoNumber = params.newsPromoNumber;
      promo.newsPromolocation = params.newsPromolocation;
      promo.newsPromoDescription = params.newsPromoDescription;

      promo.save(function(err, promoS) {
        if (err) {
          return callback(err);
        }
        callback(promoS);
      });
    });
  },

  deletePromo: function(promo_id, callback) {
    Promo.remove({
      _id: promo_id
    }, function(err, promo) {
      if (err) {
        callback({
          status: false,
          message: err
        });
        return;
      }

      callback({
        status: true,
        message: 'Promo berhasil dihapus.'
      });
    });
  }
}


module.exports = promoLogic;
