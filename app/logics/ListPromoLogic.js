const ListPromo = require('../models/list-promo.model');

var listPromoLogic = {
  //ambil semua company untuk admin
  findAllListPromo: function(callback) {
    ListPromo.find(function(err, listPromos) {
      if (err) {
        return callback(err);
      }

      callback(listPromos);
    });
  },

    findByIdPromos: function(promo_id, callback) {
      ListPromo.findById(promo_id, function(err, res) {
        if (err) {
          callback({
            status: false,
            message: err
          });
          return;
        }

        callback({
          status: true,
          message: res
        });
      });
    },

  findAllByIdPromo: function(promo_id, callback) {
    ListPromo.find({
      promo_id: promo_id
    })
      .exec(function(err, listPromos) {
        if (err) {
          return callback(err);
        }

        callback(listPromos);
      });
  },

  saveListPromo: function(params, callback) {
    var listPromo = new ListPromo({
      listNewsPromoName: params.listNewsPromoName,
      listNewsPromoImage: params.listNewsPromoImage,
      liststartDate: params.liststartDate,
      listoutDate: params.listoutDate,
      liststatus: params.liststatus,
      listNewsCategory: params.listNewsCategory,
      listNewsPromoDescription: params.listNewsPromoDescription,
      promo_id: params.promo_id
    });
    listPromo.save(function(err, listPromos) {
      if (err) {
        return callback(err);
      }
      callback(listPromos);
    });
  },



  updateListPromo: function(listPromo_id, params, callback) {
    ListPromo.findById(listPromo_id, function(err, listPromo) {
      listPromo.listNewsPromoName = params.listNewsPromoName;
      listPromo.listNewsPromoImage = params.listNewsPromoImage;
      listPromo.liststartDate = params.liststartDate;
      listPromo.listoutDate = params.listoutDate;
      listPromo.liststatus = params.liststatus;
      listPromo.listNewsCategory = params.listNewsCategory;
      listPromo.listNewsPromoDescription = params.listNewsPromoDescription;
      listPromo.promo_id = params.promo_id;

      listPromo.save(function(err, listPromos) {
        if (err) {
          return callback(err);
        }
        callback(listPromos);
      });
    });
  },

  deleteListPromo: function(listPromo_id, callback) {
    ListPromo.remove({
      _id: listPromo_id
    }, function(err, listPromo) {
      if (err) {
        callback({
          status: false,
          message: err
        });
        return;
      }

      callback({
        status: true,
        message: 'List Promo berhasil dihapus.'
      });
    });
  }
}

//   //update logo company
//   updateLogoCompany: function(company_id, filename, callback) {
//     Company.findById(company_id, function(err, company) {
//       company.company_logo = filename;
//       company.save(function(err, companyS) {
//         if (err) {
//           return callback(err);
//         }
//         callback(companyS);
//       });
//     });
//   }
//
// }

module.exports = listPromoLogic;
