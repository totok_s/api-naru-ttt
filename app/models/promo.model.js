var mongoose = require('mongoose');

var PromoSchema = mongoose.Schema({
  newsPromoName: {
    type: 'String',
    required: true
  },
  newsPromoImage: [{
    type: 'String',
    required: true
  }],
  startDate: {
    type: Date,
    required: true
  },
  outDate: {
    type: Date,
    required: true
  },
  status: {
    type: Boolean,
    required: true
  },
  newsCategory: {
    type: 'String',
    required: true
  },
  newsPromoDescription: {
    type: 'String',
    required: true
  }

})

module.exports = mongoose.model('Promo', PromoSchema);
