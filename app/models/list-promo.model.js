var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var objectId = Schema.Types.ObjectId;
var ListPromoSchema = mongoose.Schema({
  listNewsPromoName: {
    type: 'String',
    required: true
  },
  listNewsPromoImage: [{
    type: 'String',
    required: true
  }],
  liststartDate: {
    type: Date,
    required: true
  },
  listoutDate: {
    type: Date,
    required: true
  },
  liststatus: {
    type: Boolean,
    required: true
  },
  listNewsCategory: {
    type: 'String',
    required: true
  },
  listNewsPromoDescription: {
    type: 'String',
    required: true
  },
  promo_id: {
    type: objectId,
    ref: 'Promo'
  }

})

module.exports = mongoose.model('ListPromo', ListPromoSchema);
